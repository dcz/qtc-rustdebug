Rust helpers for QtCreator
=============

Those helpers teach QtCreator's debugger to display the Rust data structures correctly.

Instead of fields like __1 and __2, QtCreator will actually display the correct variant for enums:

![enum variant](i/enum.png)

![option](i/option.png)

the length and contents of strings and vectors:

![vector debugged](i/strvec.png)

![string](i/string.png)

and format byte sequences in hex, fo your convenience:

![fancy u8 print](i/fancy_printer.png)

It even handles nesting more or less correctly:

![newtype nested in an enum](i/nested.png)

Usage
------

Tools → Options → Debugger → Locals & Expressions

Make sure that "Use Debugging Helpers" is checked. In the field "Extra Debugging Helper", insert the path to the helpers.py file, like this: `/home/foo/qtc-rustdebug/helpers.py`.

Load any Rust program you want, and debug it. Easiest is to use provided "test.rs":

```
rustc test.rs
```

Then go back to QtCreator and:

Debug → Start Debugging → Start and Debug External Application

In the dialog's "Local executable", enter the path to the "test" binary: `/home/foo/qtc-rustdebug/test`. Check "Break at "main"". Click "OK". Wait for the program to run, and press "Step into" (F11) a few times.

Interference
----------

Those helpers *should not* interfere with code written in other languages. As long as classes have different names, of course. That means C++ code should be safe.

Stability
--------

This has been tested with rustc 1.69.0. Because the debugger needs matching debugging information from the binary, and because the exact encoding of debug information depends on the compiler version, it's possible that code produced with other versions of rustc will not get detected properly. Rust does not have a stable ABI to prevent this.

Code quality
--------------

The QtCreator's builtin helper is quite buggy, but it's hard to rip rewrite it. Instead, we take advantag of it being written in Python, and we monkey-patch some bad methods with ones that work better.

Debugging
----------

The helper can leave a file in a selected directory, and that file will contain any output. Check out the "out" variable near the line `from sys import stdout as out`.

Also, enable the debugger log in QtCreator:

View → Views → Debugger Log

License
------

Because this uses portions of QtCreator's own debugging helpers, it's GPLv3.