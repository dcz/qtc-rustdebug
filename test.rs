#[derive(Debug)]
enum MapNode {
    key(u32),
    data(Newtype),
    nothing,
}

#[derive(Debug)]
enum FOption<T> {
    FSome(T),
    FNone,
}

#[derive(Debug)]
struct Newtype(usize);

#[derive(Debug)]
struct S {
    data: Newtype,
    nothing: (),
}

#[inline(never)]
fn printd(t: MapNode) {
    println!("{:?}", t);
}
#[inline(never)]
fn print(t: &str) {
    println!("{}", t);
}
#[inline(never)]
fn printS(t: String) {
    println!("{}", t);
}
#[inline(never)]
fn printV(t: Vec<&str>) {
    println!("{:?}", t);
}

fn printVi(t: Vec<u32>) {
    println!("{:?}", t);
}
#[inline(never)]
fn printo(t: Option<&str>) {
    println!("{:?}", t);
}
#[inline(never)]
fn printio(t: Option<u32>) {
    println!("{:?}", t);
}
#[inline(never)]
fn printfo(t: FOption<u32>) {
    println!("{:?}", t);
}
#[inline(never)]
fn printn(n: Newtype) {
    println!("{:?}", n);
}

fn printi(n: u32) {
    println!("{:?}", n);
}

fn printSt(s: S) {
    println!("{:?}", s);
}

fn printRef(s: &S) {
    println!("{:?}", s);
}

fn printSl(s: &[u8]) {
    println!("{:?}", s);
}


fn main() {
    printi(44);
    print(String::from("foo").as_str());
    printS(String::from("foo"));
    printn(Newtype(5));
    printd(MapNode::nothing);
    printd(MapNode::key(45));
    printd(MapNode::data(Newtype(45)));
    printV(vec!["foo", "bar"]);
    printVi(vec![1, 2]);
    printSl(&[5, 6][..]);
    printo(Some("foo"));
    printo(None);
    printio(Some(33));
    printio(Some(0));
    printio(None);
    printfo(FOption::FSome(33));
    printfo(FOption::FSome(0));
    printfo(FOption::FNone);
    printSt(S {
        data: Newtype(55),
        nothing: (),
    });
    printRef(&S {
        data: Newtype(55),
        nothing: (),
    });
}
