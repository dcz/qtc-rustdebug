'''
Rust helpers for QtCreator.

Based on QtCreator's builtin debugger helpers.
Docs at https://doc.qt.io/qtcreator/creator-debugging-helpers.html

Exceptions on module load show in up global debugger log in QtCreator,
but if one appears suddenly, there will be no information
and old code will just keep getting executed when clicking "Reload helpers".
You need to stop the debugger and start it again for the exception to show up.

Copying: follow the GPLv3 license.
'''

'''
TypeCode:
        Typedef,
        Struct,
        Void,
        Integral,
        Float,
        Enum,
        Pointer,
        Array,
        Complex,
        Reference,
        Function,
        MemberPointer,
        FortranString,
        Unresolvable,
        Bitfield,
        RValueReference,

{'TYPE_CODE_ARRAY': 2, 'TYPE_CODE_BITSTRING': -1, 'TYPE_CODE_BOOL': 21, 'TYPE_CODE_CHAR': 20, 'TYPE_CODE_COMPLEX': 22, 'TYPE_CODE_DECFLOAT': 25, 'TYPE_CODE_ENUM': 5, 'TYPE_CODE_ERROR': 14, 'TYPE_CODE_FLAGS': 6, 'TYPE_CODE_FLT': 9, 'TYPE_CODE_FUNC': 7, 'TYPE_CODE_INT': 8, 'TYPE_CODE_INTERNAL_FUNCTION': 27, 'TYPE_CODE_MEMBERPTR': 17, 'TYPE_CODE_METHOD': 15, 'TYPE_CODE_METHODPTR': 16, 'TYPE_CODE_NAMESPACE': 24, 'TYPE_CODE_PTR': 1, 'TYPE_CODE_RANGE': 12, 'TYPE_CODE_REF': 18, 'TYPE_CODE_RVALUE_REF': 19, 'TYPE_CODE_SET': 11, 'TYPE_CODE_STRING': 13, 'TYPE_CODE_STRUCT': 3, 'TYPE_CODE_TYPEDEF': 23, 'TYPE_CODE_UNION': 4, 'TYPE_CODE_VOID': 10}
'''

import gdb
import socket

import dumper
from dumper import *

class TypeCode():
    (
        Typedef,
        Struct,
        Void,
        Integral,
        Float,
        Enum,
        Pointer,
        Array,
        Complex,
        Reference,
        Function,
        MemberPointer,
        FortranString,
        Unresolvable,
        Bitfield,
        RValueReference,
        # new additions
        Union,
    ) = range(0, 17)

class Value(DumperBase.Value):
    def extractField(self, field):
        if not isinstance(field, self.dumper.Field):
            raise RuntimeError('BAD INDEX TYPE %s' % type(field))

        if field.extractor is not None:
            val = field.extractor(self)
            if val is not None:
                #DumperBase.warn('EXTRACTOR SUCCEEDED: %s ' % val)
                return val

        if self.type.code == TypeCode.Typedef:
            return self.cast(self.type.ltarget).extractField(field)
        if self.type.code in (TypeCode.Reference, TypeCode.RValueReference):
            return self.dereference().extractField(field)
        #DumperBase.warn('FIELD: %s ' % field)
        val = self.dumper.Value(self.dumper)
        val.name = field.name
        val.isBaseClass = field.isBase
        val._type = field.fieldType()

        if field.isArtificial:
            if self.laddress is not None:
                val.laddress = self.laddress
            if self.ldata is not None:
                val.ldata = self.ldata
            return val

        fieldBitsize = field.bitsize
        fieldSize = (fieldBitsize + 7) // 8
        fieldBitpos = field.bitpos
        fieldOffset = fieldBitpos // 8
        fieldType = field.fieldType()

        if fieldType.code == TypeCode.Bitfield:
            fieldBitpos -= fieldOffset * 8
            ldata = self.data()
            data = 0
            for i in range(fieldSize):
                data = data << 8
                if self.dumper.isBigEndian:
                    lbyte = ldata[i]
                else:
                    lbyte = ldata[fieldOffset + fieldSize - 1 - i]
                if sys.version_info[0] >= 3:
                    data += lbyte
                else:
                    data += ord(lbyte)
            data = data >> fieldBitpos
            data = data & ((1 << fieldBitsize) - 1)
            val.lvalue = data
            val.laddress = None
            return val

        if self.laddress is not None:
            val.laddress = self.laddress + fieldOffset
        elif self.ldata is not None:
            val.ldata = self.ldata[fieldOffset:fieldOffset + fieldSize]
        else:
            pass # Rust enum variants don't necessarily have laddress or ldata

        if fieldType.code in (TypeCode.Reference, TypeCode.RValueReference):
            if val.laddress is not None:
                val = self.dumper.createReferenceValue(val.laddress, fieldType.ltarget)
                val.name = field.name

        #DumperBase.warn('GOT VAL %s FOR FIELD %s' % (val, field))
        val.lbitsize = fieldBitsize
        val.check()
        return val

DumperBase.Value = Value

def debug(dumper):
    def curry(arg):
        def c(f):
            def g(*args, **kwargs):
                return f(arg, *args, **kwargs)
            return g
        return c
    
    def override_method(dumper):
        def g(f):
            curried = curry(dumper)(f)
            setattr(dumper, f.__name__, curried)
            return curried
        return g

    @override_method(dumper)
    def listMembers(self, value, nativeType):
        nativeValue = value.nativeValue
        # a value coming from GDB may eliminate unused fields from the general instance of the type. The type coming in here is cached from such value, so this uncaches it.
        if value.nativeValue:
            nativeType = value.nativeValue.type

        anonNumber = 0
        for nativeField in nativeType.fields():
            fieldName = nativeField.name
            # Something without a name.
            # Anonymous union? We need a dummy name to distinguish
            # multiple anonymous unions in the struct.
            # Since GDB commit b5b08fb4 anonymous structs get also reported
            # with a 'None' name.
            if fieldName is None or len(fieldName) == 0:
                # Something without a name.
                # Anonymous union? We need a dummy name to distinguish
                # multiple anonymous unions in the struct.
                anonNumber += 1
                fieldName = '#%s' % anonNumber
            #DumperBase.warn('FIELD: %s' % fieldName)
            # hasattr(nativeField, 'bitpos') == False indicates a static field,
            # but if we have access to a nativeValue .fromNativeField will
            # also succeed. We essentially skip only static members from
            # artificial values, like array members constructed from address.
            if hasattr(nativeField, 'bitpos') or nativeValue is not None:
                yield self.fromNativeField(nativeField, nativeValue, fieldName)
    
    @override_method(dumper)
    def tryPutPrettyItem(self, typeName, value):
        """In comparison to the standard one, this gives up on the special path when the dumper returns False"""
        value.check()
        if self.useFancy and self.currentItemFormat() != DisplayFormat.Raw:
            self.putType(typeName)

            nsStrippedType = self.stripNamespaceFromType(typeName)\
                .replace('::', '__')

            #DumperBase.warn('STRIPPED: %s' % nsStrippedType)
            # The following block is only needed for D.
            if nsStrippedType.startswith('_A'):
                # DMD v2.058 encodes string[] as _Array_uns long long.
                # With spaces.
                if nsStrippedType.startswith('_Array_'):
                    qdump_Array(self, value)
                    return True
                if nsStrippedType.startswith('_AArray_'):
                    qdump_AArray(self, value)
                    return True

            dumper = self.qqDumpers.get(nsStrippedType)
            #DumperBase.warn('DUMPER: %s' % dumper)
            if dumper is not None:
                dumper(self, value)
                return True

            for pattern, dumper in self.qqDumpersEx.items():
                if re.match(pattern, nsStrippedType):
                    # THIS is needed so that dumpers can fail
                    if dumper(self, value):
                        return True
        return False

    @override_method(dumper)
    def fromNativeType(self, nativeType):
        self.check(isinstance(nativeType, gdb.Type))
        code = nativeType.code
        #DumperBase.warn('FROM NATIVE TYPE: %s' % nativeType)
        nativeType = nativeType.unqualified()

        if code == gdb.TYPE_CODE_PTR:
            #DumperBase.warn('PTR')
            targetType = self.fromNativeType(nativeType.target().unqualified())
            return self.createPointerType(targetType)

        if code == gdb.TYPE_CODE_REF:
            #DumperBase.warn('REF')
            targetType = self.fromNativeType(nativeType.target().unqualified())
            return self.createReferenceType(targetType)

        if hasattr(gdb, "TYPE_CODE_RVALUE_REF"):
            if code == gdb.TYPE_CODE_RVALUE_REF:
                #DumperBase.warn('RVALUEREF')
                targetType = self.fromNativeType(nativeType.target())
                return self.createRValueReferenceType(targetType)

        if code == gdb.TYPE_CODE_ARRAY:
            #DumperBase.warn('ARRAY')
            nativeTargetType = nativeType.target().unqualified()
            targetType = self.fromNativeType(nativeTargetType)
            if nativeType.sizeof == 0:
                # QTCREATORBUG-23998, note that nativeType.name == None here,
                # whereas str(nativeType) returns sth like 'QObject [5]'
                count = self.arrayItemCountFromTypeName(str(nativeType), 1)
            else:
                count = nativeType.sizeof // nativeTargetType.sizeof
            return self.createArrayType(targetType, count)

        if code == gdb.TYPE_CODE_TYPEDEF:
            #DumperBase.warn('TYPEDEF')
            nativeTargetType = nativeType.unqualified()
            while nativeTargetType.code == gdb.TYPE_CODE_TYPEDEF:
                nativeTargetType = nativeTargetType.strip_typedefs().unqualified()
            targetType = self.fromNativeType(nativeTargetType)
            return self.createTypedefedType(targetType, str(nativeType),
                                            self.nativeTypeId(nativeType))

        if code == gdb.TYPE_CODE_ERROR:
            self.warn('Type error: %s' % nativeType)
            return self.Type(self, '')

        typeId = self.nativeTypeId(nativeType)
        res = self.typeData.get(typeId, None)
        if res is None:
            tdata = self.TypeData(self)
            tdata.name = str(nativeType)
            tdata.typeId = typeId
            tdata.lbitsize = nativeType.sizeof * 8
            tdata.code = {
                #gdb.TYPE_CODE_TYPEDEF : TypeCode.Typedef, # Handled above.
                gdb.TYPE_CODE_METHOD: TypeCode.Function,
                gdb.TYPE_CODE_VOID: TypeCode.Void,
                gdb.TYPE_CODE_FUNC: TypeCode.Function,
                gdb.TYPE_CODE_METHODPTR: TypeCode.Function,
                gdb.TYPE_CODE_MEMBERPTR: TypeCode.Function,
                #gdb.TYPE_CODE_PTR : TypeCode.Pointer,  # Handled above.
                #gdb.TYPE_CODE_REF : TypeCode.Reference,  # Handled above.
                gdb.TYPE_CODE_BOOL: TypeCode.Integral,
                gdb.TYPE_CODE_CHAR: TypeCode.Integral,
                gdb.TYPE_CODE_INT: TypeCode.Integral,
                gdb.TYPE_CODE_FLT: TypeCode.Float,
                gdb.TYPE_CODE_ENUM: TypeCode.Enum,
                #gdb.TYPE_CODE_ARRAY : TypeCode.Array,
                gdb.TYPE_CODE_STRUCT: TypeCode.Struct,
                gdb.TYPE_CODE_UNION: TypeCode.Struct,
                gdb.TYPE_CODE_COMPLEX: TypeCode.Complex,
                gdb.TYPE_CODE_STRING: TypeCode.FortranString,
                gdb.TYPE_CODE_NAMESPACE: TypeCode.Enum,
            }[code]
            if tdata.code == TypeCode.Enum:
                tdata.enumDisplay = lambda intval, addr, form: \
                    self.nativeTypeEnumDisplay(nativeType, intval, form)
            if tdata.code == TypeCode.Struct:
                tdata.lalignment = lambda: \
                    self.nativeStructAlignment(nativeType)
                tdata.lfields = lambda value: \
                    self.listMembers(value, nativeType)
            tdata.templateArguments = self.listTemplateParameters(nativeType)
            self.registerType(typeId, tdata)  # Fix up fields and template args
        #    warn('CREATE TYPE: %s' % typeId)
        #else:
        #    warn('REUSE TYPE: %s' % typeId)
        return self.Type(self, typeId)

    return dumper

from gdbbridge import Dumper
from pprint import pformat

print("Rust helper loaded")

#out = open('/home/foo/o', 'w')
from sys import stdout as out

def eprint(*args, file=None):
    print(*args, file=out)
    out.flush()

def ptrSize(self):
    return 8

# monkey-patch the dumper because Rust doesn't define the void type
Dumper.ptrSize = ptrSize

def extractPointer(value):
    return value.extractSomething('Q', 8 * 8)

def extractStr(d, ptr, l):
    return bytes(d.readRawMemory(ptr, l)).decode('utf-8')

def represent(d, value):
    if value.type.code == TypeCode.Integral:
        return value.lvalue

def todict(value):
    if isinstance(value, DumperBase.Type):
        return str(value)
    if isinstance(value, DumperBase.Value):
        d = todir(value)

        d['type'] = todict(d['type'])
        return d
    else:
        return value


def value_members(value):
    return {
        'name': value.name,
        'type': str(value.type),
        'members': [(v.name, value_members(v)) for v in value.members(True)]
    }


def native_type_members(t):
    d = to_dir(t)
    #d = {'name': t.name}
    
    try:
        d['members'] = [(f.name, native_type_members(f.type)) for f in t.fields()]
    except TypeError as e:
        pass
    return d


def try_dump(f):
    def g(*args, **kwargs):
        try:
            return f(*args, **kwargs)
        except Exception as e:
            import traceback
            traceback.print_exception(e, file=out)
            eprint("done")
            raise
    return g


def qdump__alloc__string__String(d, value):
    l = value['vec']['len'].integer()
    d.putValue(extractStr(d, extractPointer(value['vec']['buf']['ptr']), l))
    return True

def _qdump__string_slice(d, value, regex='^\&str$'):
    l = value['length'].integer()
    #eprint("{:04x}".format(extractPointer(value['data_ptr'])))
    d.putValue(extractStr(d, extractPointer(value['data_ptr']), l))
    return True

@try_dump
def _qdump__core__option__Option(d, value):
    return dump_enum(d, value)

def qdump__core__option__Option(d, value):
    return _qdump__core__option__Option(d, value)

@try_dump
def _qdump__core__net__ip_addr__Ipv6Addr(d, value):
    d.putValue(socket.inet_ntop(
        socket.AF_INET6,
        d.readRawMemory(value.laddress, 16)
    ))
    d.putType(value.type)
    return True
    
def qdump__core__net__ip_addr__Ipv6Addr(d, value):
    return _qdump__core__net__ip_addr__Ipv6Addr(d, value)
    

def dump__slice(d, value):
    l = value['length'].integer()
    child_type = value.type.name[2:-1]
    if child_type == 'u8':
        d.putValue(
            '{}B: '.format(l) + d.readRawMemory(extractPointer(value['data_ptr']), l)
                .hex()
        )
    else:
        d.putItemCount(l)
        d.setExpandable()
    child_type = d.createType(child_type)
    item_size = child_type.size()
    p = extractPointer(value['data_ptr'])
    
    if d.isExpanded():
        d.putPlotData(p, l, child_type)
    return True

@try_dump
def _qdump__alloc__vec__Vec(d, value):
    l = value['len'].integer()
    subtypes = value.type.name[len("alloc::vec::Vec<"):-1]
    # FIXME: alloc type may contain comma in parameters. Parse properly
    child_type, alloc = subtypes.rsplit(',', maxsplit=1)
    child_type = d.createType(child_type)
    d.putItemCount(l)
    item_size = child_type.size()
    p = extractPointer(value['buf']['ptr'])
    
    if d.isExpanded():
        d.putPlotData(p, l, child_type)
    return True

def qdump__alloc__vec__Vec(d, value):
    return _qdump__alloc__vec__Vec(d, value)


def dump_enum(d, value):
    members = value.members(True)
    discr = members[0].integer()
    # GDB removes unused fields, so the selected enum will always be the only one
    inner = members[1]

    members = inner.members(True)
    if len(members) == 1:
        representation = represent(d, members[0])
    else:
        representation = None
    if representation:
        d.putValue('{}({})'.format(inner.name, representation))
    else:
        d.putValue(inner.name)
    
    if len(members) > 0:
        d.putExpandable()
        unnamedstruct_items(d, members)
    return True


def unnamedstruct_items(d, members):
    if d.isExpanded():
        with Children(d, numChild=len(members)):
            for member in members:
                with SubItem(d, member.name):
                    dump_all(d, member)


def dump_unnamedstruct(d, value):
    members = value.members(True)
    d.putName(value.name)
    d.putType(value.type)
    d.putExpandable()
    representation = represent(d, members[0])
    if len(members) == 1 and representation:
        d.putValue('{}'.format(representation))
    unnamedstruct_items(d, members)
    return True


def dump_int(d, value):
    d.putValue(value.value())
    d.putType(value.type)
    return True

def dump_struct(d, value):
    return False
# last to provide fallback?

def todir(value):
    def ga(v, k):
        try:
            if hasattr(v, k):
                return getattr(v, k)
        except Exception as e:
            return e

    return dict((k, ga(value, k)) for k in dir(value)
        if not k.startswith('__') and not inspect.isroutine(ga(value, k)))

to_dir = todir

def dbg(d):
    eprint(pformat(todir(d)))
    return d

def dump_all(d, value):
    if not dump_anything(d, value):
        d.putItem(value.name, value)

@try_dump
def dump_anything(d, value):
    #eprint(pformat(todict(value)))
    #eprint(pformat(todict(value._type)))
    #eprint(pformat(todict(value.to())))
    eprint(value.type)
    try:
        eprint(pformat(value_members(value)))
    except Exception as e:
        traceback.print_exception(e, file=out)
        eprint("Continuing")
    '''
    if value.type.name == 'test::MapNode':
        import gdb
        dbg(gdb.lookup_type('test::MapNode'))
        eprint(
            "lowlevel",
            pformat(native_type_members(value.nativeValue.type))
        )
        eprint(
            "fields",
            pformat([to_dir(f) for f in value.type.typeData().lfields(value)])
        )
    '''
    if value.type.code == TypeCode.Integral:
        return dump_int(d, value)
    #eprint(value.nativeValue)
    #eprint(pformat(
        #dict((name, getattr(value, name)) for name in dir(value.nativeValue))))
    # This captures all, so let's make a second attempt to redirect to the right handler here.s
    
    if value.name == 'alloc::Vec::Vec':
        return qdump__alloc__vec__Vec(d, value)
    elif value.name == 'core::option::Option':
        return qdump__core__option__Option(d, value)
    elif value.name == 'alloc::string::String':
        return qdump__alloc__string__String(d, value)
    elif value.type.name and value.type.name.startswith('&[') and value.type.name.endswith(']'):
        return dump__slice(d, value)
    elif value.type.name == '&str':
        return _qdump__string_slice(d, value)
    elif value.type.code == TypeCode.Struct:
        members = value.members(True)
        names = [v.name for v in members]

        if '#1' in names:
            return dump_enum(d, value)
        elif '__0' in names:
            return dump_unnamedstruct(d, value)
        return dump_struct(d, value)
    return False

def qdump__anything(d, value, regex='^.*'):
    d = debug(d)
    return dump_anything(d, value)